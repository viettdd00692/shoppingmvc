<%-- 
    Document   : index
    Created on : Dec 16, 2019, 9:00:48 AM
    Author     : Viet
--%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Order Product</title>
    </head>
    <body bgcolor="#FFFFCC">
        <h1>Welcome to Shop Stop !!!</h1>
        <hr/>
        <jsp:useBean id="prod" class="model.ProductCart" scope="session"/>
        <form action="ShoppingServlet" name="shoppingForm" method="POST">
            <b> Products </b> <br>
            <select name="products">
                <c:forEach var="item" items="${prod.products}">
                    <option>
                        ${item.productId}${"|"}${item.productName}${"|"}${item.productType}${"|"}${item.price}
                    </option>
                </c:forEach>
            </select>
            <br> <br>
            <b> Quantity </b> <input type="text" name="qty" value="1"> <br>
            <input type="hidden" name="action" value="ADD">
            <input type="submit" name="Submit" value="Add to Cart">
        </form>
        <p>${message}</p>
        <c:if test="${size == 1}">
            <jsp:include page="cart.jsp" flush="true"/>
        </c:if>
    </body>
</html>
