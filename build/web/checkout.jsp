<%-- 
    Document   : checkout
    Created on : Dec 16, 2019, 10:01:05 AM
    Author     : Viet
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<jsp:useBean id="prod" class="model.ProductCart" scope="session"/>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Transaction</title>
    </head>
    <body bgcolor="#FFFFCC">
        <h1>Transaction Details.</h1>
        <center>
            <table>
                <tr>
                    <td> <b> Product Id </b> </td>
                    <td> <b> Product Name </b> </td>
                    <td> <b> Product Type </b> </td>
                    <td> <b> Price </b> </td>
                    <td> <b> Quantity </b> </td>
                </tr>
                <c:forEach var="item" items="${prod.cartItems}">
                <tr>
                    <td>${item.productId} </td>
                    <td>${item.productName} </td>
                    <td>${item.productType} </td>
                    <td>${item.price} </td>
                    <td>${item.quantity} </td>
                </tr>
                </c:forEach>
                <tr>
                    <td></td>
                    <td></td>
                    <td><b> Total </b></td>
                    <td><b> ${prod.amount} </b></td>
                    <td></td>
                </tr>
            </table>
            <br/><br/>
            <h3>Customer Information</h3>
            <form action="ShoppingServlet" name="proceedForm" method="POST">
                Name: <input type="text" name="name"/><br/><br/>
                Email: <input type="text" name="email"/><br/><br/>
                Phone: <input type="text" name="phone"/><br/><br/>
                <a href="index.jsp">Back</a>
                <input type="hidden" name="action" value="PROCEED"/>
                <input type="submit" name="Proceed" value="Proceed"/>
            </form>
            <br>
        </center>
    </body>
</html>
