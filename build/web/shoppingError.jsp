<%-- 
    Document   : shoppingError
    Created on : Dec 16, 2019, 9:59:23 AM
    Author     : Viet
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Shopping</title>
    </head>
    <body>
        <h1>Error Occurred</h1>
        ${message}
        <a href="index.jsp"> Home </a>
    </body>
</html>
